import { Head } from 'next/document'
import theme from 'src/styles/theme'

const Heads = () => {
  return (
    <Head>
      {/* PWA primary color */}
      <meta name="theme-color" content={theme.palette.primary.main} />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      />
    </Head>
  )
}

export default Heads
