import React from 'react'
import Link from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'
import LightBulbIcon from 'src/icons/LightBulbIcon'
import { useProtipStyle } from 'src/styles/protip.style'

export default function ProTip() {
  const classes = useProtipStyle()
  return (
    <Typography className={classes.root} color="textSecondary">
      <LightBulbIcon className={classes.lightBulb} />
      Pro tip: See more{' '}
      <Link href="https://material-ui.com/getting-started/templates/">
        templates
      </Link>{' '}
      on the Material-UI documentation.
    </Typography>
  )
}
