# Next.js with Material UI & TypeScript as Starter Template

## Structure

```bash
├── README.md
├── next-env.d.ts
├── package.json
├── pages
│   ├── _app.tsx
│   ├── _document.tsx
│   ├── api
│   │   └── hello.ts
│   ├── example
│   │   └── about.tsx
│   └── index.tsx
├── public
│   ├── favicon.ico
│   └── vercel.svg
├── src
│   ├── components
│   │   ├── Heads.tsx
│   │   └── example
│   │       ├── Copyright.tsx
│   │       ├── Link.tsx
│   │       └── ProTip.tsx
│   ├── icons
│   │   └── LightBulbIcon.tsx
│   ├── libs
│   └── styles
│       ├── protip.style.tsx
│       └── theme.tsx
└── tsconfig.json
```

## Installation

clone this repo

```bash
git clone https://gitlab.com/ashymee.dev/next-ts-mui-template-starter.git
```

then install using `yarn` or `npm`,

```bash
yarn install

npm install
```

then run using `yarn` or `npm`

```bash
yarn dev

npm run dev
```

## Example Page & Component

this starter template is based on [official repo](https://github.com/mui-org/material-ui/tree/master/examples/nextjs-with-typescript)

The about page is located in `pages/example` directory that can be accessed in `http://localhost/3000/example/about`

And all example components are located in `src/components/example`.

So, if you use this template, you can easily modify / delete it and customize it as needed.
